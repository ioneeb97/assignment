import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public email: string;
  public password: string;
  public rememberMe: boolean;


  constructor() { }

  ngOnInit(): void {
    this.email = "";
    this.password = "";
    this.rememberMe = true;
  }

  onRememberMeChange(event){
    this.rememberMe = event.target.checked;
    console.log(this.rememberMe);
  }

  submitCredentials(){
    console.log(`Email: ${this.email} Password: ${this.password}`);
  }
}
